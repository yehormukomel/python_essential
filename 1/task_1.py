"""
Завдання 1

Створіть клас, який описує книгу. Він повинен містити інформацію про автора,
назву, рік видання та жанр. Створіть кілька різних книжок. Визначте для нього
методи _repr_ та _str_.
"""


class Book:
    def __init__(self, author, name, year, genre):
        self.author = author
        self.book_name = name
        self.year = year
        self.genre = genre

    def __str__(self):
        return f"Автор: {self.author}\n" \
               f"Назва: {self.book_name}\n" \
               f"Рік видання: {self.year}\n" \
               f"Жанр: {self.genre}"

    def __repr__(self):
        return f"Автор: {self.author}\n" \
               f"Назва: {self.book_name}\n" \
               f"Рік видання: {self.year}\n" \
               f"Жанр: {self.genre}"


book_1 = Book(author='Bertrand Russell',
              name='A History of Western Philosophy',
              year=1945,
              genre='non-fiction')

book_2 = Book(author='Howard Phillips Lovecraft',
              name='The Case of Charles Dexter Ward',
              year=1941,
              genre='horror')

book_3 = Book(author='Bertrand Russell',
              name='The Principles of Mathematics',
              year=1951,
              genre='Mathematics')

print(book_1)
print()
print(book_2)
print()
print(book_3)
print()
