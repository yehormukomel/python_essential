"""
Завдання 4

Створіть клас, який описує автомобіль. Створіть клас автосалону,
що містить в собі список автомобілів, доступних для продажу,
і функцію продажу заданого автомобіля.
"""


class Car:
    def __init__(self, brand, model, year, price):
        self.brand = brand
        self.model = model
        self.year = year
        self.price = price

    def __str__(self):
        return f"Бренд: {self.brand}\n" \
               f"Модель: {self.model}\n" \
               f"Рік: {self.year}\n" \
               f"Ціна: {self.price}"

    def __repr__(self):
        return f"Бренд: {self.brand}\n" \
               f"Модель: {self.model}\n" \
               f"Рік: {self.year}\n" \
               f"Ціна: {self.price}"


class CarShowroom:
    def __init__(self, cars: list):
        self.cars = cars

    def __str__(self):
        return f"Всі наявні автомобілі:\n"\
               + '\n\n'.join(map(lambda x: str(x), self.cars))

    def sell_car(self, brand, model):
        for car in self.cars:
            if car.brand == brand and car.model == model:
                print("Продано: ")
                print(str(car))
                self.cars.remove(car)
                print()


car_1 = Car(brand='lamborghini', model='Huracan evo', year=2021, price=40000)
car_2 = Car(brand='bmw', model='i8', year=2017, price=100000)
car_3 = Car(brand='subaru', model='forester', year=2019, price=100)

car_showroom = CarShowroom(cars=[car_1, car_2, car_3])
print(car_showroom)
print()
car_showroom.sell_car(brand='subaru', model='forester')
print(car_showroom)
