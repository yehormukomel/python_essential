"""
Завдання 2

Створіть клас, який описує відгук до книги.
Додайте до класу книги поле – список відгуків.
Зробіть так, щоб при виведенні книги на екран за допомогою функції
print також виводилися відгуки до неї.
"""


class Book:
    def __init__(self, author, name, year, genre):
        self.author = author
        self.book_name = name
        self.year = year
        self.genre = genre
        self.book_reviews = []

    def __str__(self):
        return f"Автор: {self.author}\n" \
               f"Назва: {self.book_name}\n" \
               f"Рік видання: {self.year}\n" \
               f"Жанр: {self.genre}" \
               + '\n\nВідгуки: \n' \
               + '\n'.join(review for review in self.book_reviews)

    def add_review(self, user_name, review):
        self.book_reviews.append(str(Review(user_name=user_name, review=review)))


class Review:
    def __init__(self, user_name, review):
        self.user_name = user_name
        self.review = review

    def __str__(self):
        return f"Користувач: {self.user_name}\nВідгук: {self.review}"


book_1 = Book(author='Bertrand Russell',
              name='A History of Western Philosophy',
              year=1945,
              genre='non-fiction')

book_2 = Book(author='Howard Phillips Lovecraft',
              name='The Case of Charles Dexter Ward',
              year=1941,
              genre='horror')

book_3 = Book(author='Bertrand Russell',
              name='The Principles of Mathematics',
              year=1951,
              genre='Mathematics')

book_1.add_review('dimon666', "Рекомендую, на 1 курсі саме воно")
book_1.add_review('jdex', "Дуже  цікава річ")
book_2.add_review('vovan', 'Блін, страшно!')
book_2.add_review('ggghhh', '@vovan, а нашо ти це взагалі її купував, якщо боїшся')

print(book_1)
print()
print(book_2)
